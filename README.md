[![Java](https://img.shields.io/badge/language-java-orange.svg?style=flat
)](https://www.oracle.com/java/technologies/javase-downloads.html)
[![License](https://img.shields.io/badge/license-AGPLv3-blue.svg?style=flat
)](https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE)

# Omnicraft

![A screenshot of Omnicraft, showing different sizes for entities](https://cdn.discordapp.com/attachments/868642234436829194/1060999705292718150/Omnicraft208.png)

Omnicraft is a 3d Voxel game, intended to allow for an immersive, dynamic survival sandbox experience. It's currently more of a tech demo than a game, but development is ongoing. This repo contains a build script to automatically download and compile the games modules.

## About
### Developers
- @AngularAngel
### other Contributors
- <img src="https://avatars.githubusercontent.com/u/6981448" width="20" height="20">[Smashmaster](https://github.com/SmashMaster) - Maintains DevilUtil library, and contributes lots of advice.
- <img src="https://avatars.githubusercontent.com/u/43880493" width="20" height="20">[QuantumDeveloper](https://github.com/IntegratedQuantum) - Has helped solve numerous technical problems.
### The game
- The development started on September 30, 2020, see thread for screenshots and updates: https://anticapitalist.party/@Angle/104956154486767889
- Omnicraft runs on lwjgl and [DevilUtil](https://github.com/SmashMaster/DevilUtil), and uses its own voxel engine.
- This game is under AGPLv3 license for more details check the [LICENSE](https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE) file.
- You can receive announcements about Omnicraft on its [discord](https://discord.gg/q7Unc868bD) server.

# Run Omnicraft
## Compile from source:
Omnicraft is tested to compile and run with Gradle.
### Gradle
0. Install `git`.
1. Download this project.
2. Run './gradlew build run' on Linux or 'gradlew.bat build run' on Windows.

## Requirements
A computer that can run Java and a decent graphics card.
