
##Refactor Request

#Refactor Description:

What do you want documented, and how? Code commented, wiki updated, profiling done, etc?

#Examples

Are there any examples of this kind of documentation being done you'd like to point to, or of what the documentation should look like afterwards?

#Supporting Reasoning

Tell us why you think this documentation would be beneficial!

#Extra Details

Anything not put above can go here.