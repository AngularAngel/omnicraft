
##Optimization Request

#Optimization Description:

Tell us what kind of optimization you are requesting! Less memory used? Less lag? What circumstances?

#Steps to Reproduce

Tell us how to reproduce the conditions you want optimized!

#Version Information

Tell us what version of the game and of your operating system, and possible graphics card, you are using!

#Extra Details

Anything not put above can go here.