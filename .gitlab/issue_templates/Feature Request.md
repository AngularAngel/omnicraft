
##Feature Request

#Feature Description:

What new feature do you want implemented? Give us a rundown of how it works.

#Examples

Are there any examples of this feature in action you can point too? If so, tell us about them.

#Supporting Reasoning

Tell us why you think this feature would be beneficial!

#Extra Details

Anything not put above can go here.