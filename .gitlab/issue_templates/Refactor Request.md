
##Refactor Request

#Refactor Description:

What code do you want refactored, and how?

#Examples

Are there any examples of this kind of refactor being done you'd like to point to, or of what the code should look like afterwards?

#Supporting Reasoning

Tell us why you think this refactor would be beneficial!

#Extra Details

Anything not put above can go here.