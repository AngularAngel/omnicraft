
##Bug Report

#Bug Description:

What is this bug? How and where does it happen?

#Steps to Reproduce

Tell us how to reproduce this bug!

#Version Information

Tell us what version of the game and of your operating system, and possible graphics card, you are using!

#Extra Details

Anything not put above can go here.